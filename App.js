import React from 'react';
import { View, Text, Button, StyleSheet, StatusBar } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/HomeScreen"; 
import UsersScreen from './src/UsersScreen';
import LoginScreen from './src/LoginUserController';
import CadastroScreen from './src/CadastroScreen';


const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} 
        options={{
          title: "",
          headerTransparent: true,
          headerShown: false
        }}/>
        <Stack.Screen name="Users" component={UsersScreen} options={{title: "",headerTransparent: true,headerShown: false}}/>
        <Stack.Screen name="Login" component={LoginScreen} 
           options={{
            title: "",
            headerTransparent: true,
            headerShown: false
          }}/>
        <Stack.Screen name="Cadastro" component={CadastroScreen} 
           options={{
            title: "",
            headerTransparent: true,
            headerShown: false
          }}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  buttonContainer: {
    width: '80%',
    justifyContent: 'space-around',
    height: '20%',
  },
});

export default App;