import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Modal 
  
} from "react-native";
import axios from "axios";
import Icon from "react-native-vector-icons/MaterialIcons"; // Biblioteca para ícones
//import CheckDays from "./components/checkDays";
//import DateTimePicker from "./components/datePicker";

// Definição do componente UsersScreen
const UsersScreen = () => {
  // Definição dos estados utilizando hooks
  const [selectedRoomId, setSelectedRoomId] = useState(null); // const para armazenar o ID da sala selecionada
  const [reservations, setReservations] = useState([]); // const para armazenar as reservas feitas
  const [users, setUsers] = useState([]); // const para armazenar os usuários
  const [showModal, setShowModal] = useState(false); // const para controlar a exibição do modal
  // const [ schedule, setSchedule] = useState({ // const para armazenar os detalhes da reserva
  //   dateStart: "",
  //   timeStart: "",
  //   dateEnd: "",
  //   timeEnd: "",
  //   days: [],
  //   classroom: "", // Esse não precisa ser preenchida
  // });
  
  // useEffect para carregar os usuários
  useEffect(() => {
    axios
      .get("http://10.89.234.201:8081/api/users")
      .then((response) => {
        if (Array.isArray(response.data)) {
          setUsers(response.data);
        } else {
          console.error(
            "Formato inválido dos dados retornados:",
            response.data
          );
        }
      })
      .catch((error) => {
        console.error("Erro ao obter usuários:", error);
      });
  }, []);

  // Array de salas disponíveis
  const availableRooms = [
    { id: 1, name: "Sala de Português", capacity: 32, teacher: "Prof. Silva" },
    {
      id: 2,
      name: "Sala de Matemática",
      capacity: 30,
      teacher: "Prof. Santos",
    },
    { id: 3, name: "Sala de História", capacity: 25, teacher: "Prof. Costa" },
    {
      id: 4,
      name: "Sala de Biologia",
      capacity: 28,
      teacher: "Prof. Oliveira",
    },
    { id: 5, name: "Sala de Física", capacity: 27, teacher: "Prof. Rodrigues" },
    { id: 6, name: "Sala de Química", capacity: 26, teacher: "Prof. Ferreira" },
  ];

  // Função para reservar uma sala
  const reserveRoom = () => {
    setShowModal(true)
    if (selectedRoomId) {
      const room = availableRooms.find((room) => room.id === selectedRoomId);
      if (room) {
        setReservations((prevReservations) => [...prevReservations, room]);
        setSelectedRoomId(null);
      }
    }
  };

  // Função para deletar uma reserva
  const deleteReservation = (index) => {
    setReservations((prevReservations) =>
      prevReservations.filter((_, i) => i !== index)
    );
  };

  // Função para renderizar as opções de sala disponíveis
  const renderRoomOptions = () => {
    return availableRooms.map((room) => (
      <View key={room.id} style={styles.roomOptionContainer}>
        <Button
          title={room.name}
          onPress={() => setSelectedRoomId(room.id)}
          disabled={selectedRoomId === room.id}
        />
        <View style={styles.separator}></View>
      </View>
    ));
  };

  // Função para renderizar a descrição da sala selecionada
  const renderRoomDescription = () => {
    const room = availableRooms.find((room) => room.id === selectedRoomId);
    if (room) {
      return (
        <View style={styles.roomDescription}>
          <Text style={styles.roomDescriptionText}>Nome: {room.name}</Text>
          <Text style={styles.roomDescriptionText}>
            Capacidade: {room.capacity}
          </Text>
          <Text style={styles.roomDescriptionText}>
            Professor: {room.teacher}
          </Text>
          <Button title="Reservar" onPress={reserveRoom} />
        </View>
      );
    }
    return null;
  };

  // Função para renderizar as reservas feitas
  const renderReservations = () => {
    return (
      <View style={styles.reservationsBox}>
        {reservations.map((reservation, index) => (
          <View key={index} style={styles.reservationItem}>
            {/* Exibe o nome da reserva */}
            <Text style={styles.reservationText}>{reservation.name}</Text>
            {/* Botão de deletar reserva */}
            <TouchableOpacity
              style={styles.deleteButton}
              onPress={() => deleteReservation(index)}
            >
              <Icon name="delete" size={20} color="red" />
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  };

  // // Função para criar uma sala
  // const createClassroom = async() => {
  //   console.log()
  //   await api.createClassroom(schedule).then((response) =>{
  //      Alert.alert("Reserva criada com sucesso",response.data.message);
  //      setShowModal(false);
  //      setSchedule(scheduleDefault);
  //   }).catch((error) =>{
  //     Alert.alert("Erro",error.response.data.error);
  //   })
    
  // }

  return (
    <ImageBackground
      source={require("../assets/escolar.jpg")}
      style={styles.backgroundImage}
    >
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.optionsAndDescriptionContainer}>
            <View style={styles.roomOptionsContainer}>
              <Text style={styles.title}>Opções de Salas:</Text>
              {renderRoomOptions()}
            </View>
            <View style={styles.descriptionContainer}>
              <Text style={styles.descriptionTitle}>Descrição da Sala:</Text>
              {renderRoomDescription()}
            </View>
          </View>
          {renderReservations()}
          <View style={styles.usersContainer}>
            {users.map((user, index) => (
              <View key={index}>
                <Text>{user.name}</Text>
              </View>
            ))}
          </View>
          {/* Modal */}
          <Modal visible={showModal} animationType="slide" transparent={true}>
            <View style={styles.modalBackground}>
              <View style={styles.modalContainer}>
                <Text style={styles.modalTitle}>Formulário de reserva</Text>
                
                <View style={styles.buttonContainer}>
                  <Button title="Reservar " color="pink"
                //     <View>
                //     {/* Adicionando o componente DateTimePicker */}
                //     <DateTimePicker type={'date'} buttonTitle={schedule.dateStart === "" ? "Data de Início" : schedule.dateStart} dateKey={'dateStart'} setSchedule={setSchedule}/>
                //     <DateTimePicker type={'date'} buttonTitle={schedule.dateEnd === "" ? "Data de Fim" : schedule.dateEnd} dateKey={'dateEnd'} setSchedule={setSchedule} />
                //     <DateTimePicker type={'time'} buttonTitle={schedule.timeStart === "" ? "Inicio do Periodo" : schedule.timeStart} dateKey={'timeStart'} setSchedule={setSchedule}/>
                //     <DateTimePicker type={'time'} buttonTitle={schedule.timeEnd === "" ? "Fim do Periodo" : schedule.timeEnd} dateKey={'timeEnd'} setSchedule={setSchedule}/>
                //     <CheckDays selectedDays={schedule.days} setSchedule={setSchedule}></CheckDays>
                //     {/* Botão para reservar */}
                 // <TouchableOpacity style={styles.button} onPress={createClassroom}>
                //         <Text style={styles.buttonText}>Reservar</Text>
                //     </TouchableOpacity>
                // </View>
                />
                  <Button
                    title="CANCELAR"
                    color=""
                    onPress={() => setShowModal(false)}
                  />
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
  },
  content: {
    alignItems: "center",
  },
  optionsAndDescriptionContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },
  roomOptionContainer: {
    marginHorizontal: 5,
  },
  separator: {
    height: 10,
  },
  roomOptionsContainer: {
    flex: 1,
  },
  descriptionContainer: {
    flex: 1,
    marginLeft: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    borderRadius: 5,
    padding: 20,
  },
  descriptionTitle: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
  },
  roomDescription: {
    alignItems: "center",
  },
  roomDescriptionText: {
    textAlign: "center",
  },
  reservationsBox: {
    width: "50%",
    borderRadius: 10,
    padding: 10,
    alignItems: "center",
    alignSelf: "center",
  },
  reservationsTitle: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10,
  },
  reservationItem: {
    flexDirection: "row",
    alignItems: "center",
    padding: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  reservationText: {
    flex: 1,
  },
  deleteButton: {
    padding: 5,
  },
  usersContainer: {
    marginTop: 20,
  },
  usersTitle: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10,
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover",
    position: "absolute",
    width: "100%",
    height: "100%",
  },modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Define um fundo semi-transparente
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    backgroundColor: "#084d6e",
    borderRadius: 10,
    padding: 20,
    width: "80%", // Define a largura do modal
    maxHeight: "70%", // Define a altura máxima do modal
    justifyContent: "center",
    alignItems: "center",
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#fff",
  },
});

export default UsersScreen;