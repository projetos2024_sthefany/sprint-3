//imports necessários 
import react,{useState} from "react";
import {View, Button} from "react-native";
import DateTimePickerModal from 'react-native-modal-datetime-picker';


    const DateTimePicker = ({type, buttonTitle, dateKey, setSchedule}) =>{
        const [datePickerVisible,setDatePickerVisible] = useState (false) //Abrir função nativa do celular
   
        const showDatePicker = () =>{
        setDatePickerVisible(true);
        } //Alterar,exibir o status da datePicker,se for true aparece o relogio ou a data 
    
        const hideDatePicker = () =>{
        setDatePickerVisible(false);
        } //Fechar o datePicker,se for falso some 

        const handleConfirme = (date) => { 
            if(type === "time"){ 
//Lógica para extrair hora e minuto
            const hour = date.getHours();
            const minute = date.getMinutes();

//Lógica para montar hora e minuto do formato desejado
            const formattedTime = `${hour}:${minute}`;
//Atualiza o estado da reserva com HH:MM formatado
            setSchedule((prevState)=>({
                ...prevState,
                [dateKey]:formattedTime,
            }));

            } else {
                const formattedDate = date.toISOString().split('T')[0];
//Atualizar o scedule,manter o estado anterior e utilizar o dateKey para setar/atualizar 
                setSchedule((prevState)=>({
                    ...prevState,
                    [dateKey]:formattedDate,
                }));
            }
            hideDatePicker();
        };
//Contrução do component
        return( 
            <View>
                <Button title={buttonTitle} onPress={showDatePicker} color='black'/>
                <DateTimePickerModal
                isVisible={datePickerVisible} //Botão visivel 
                mode={type} //Modo tipo data hora
                locale="pt_BR"
                onConfirm={handleConfirme} //recebe data atualiza reserva 
                onCancel={hideDatePicker}//Cancelar abrir função hideDatePicker
                //Estilo opcional 
                pickerComponentStyleIOS={{backgroundColor:"black"}}
                textColor="black"
                />
            </View>
        );
};
    export default DateTimePicker;



    //dateTimePicker: pegar data e hora ou somente hora
//dateKey: usada encontrar a posição de um ojeto(Scredule),chave dentro do scredule
//setScredule: setar quando selecionar (dataInicio,dataFim,horaInicio,horaFim)
//showDatePicker: botão que por meio da função onPress vai acionar o calendario quando for (true) 
//DateTimePickerModal: chamar modal do calendario,recebe (true) vai aparecer se for (false) não vai aparecer
//...prevState: mantem o estado anterior
//buttonTitle: titulo que vai aparecer no botão,botão para selecionar()
//handleConfirme: quando confirmar inserção da data
//date: talvez seja uma hora
//getHours: função nativa para extrair hora
//getMinutes: função para extrair minuto da hora
//formattedTime: formato desejado da hora



