import axios from "axios";

const api = axios.create({
    baseURL: "http://10.89.234.173:5000/teste/",
    headers: {
        'accept': 'application/json'
    }
});

const sheets = {
    postUser: (user) => api.post("/createUsuario/", user),
    logUser: (user) => api.post("/loginUsuario/", user),

    // getAllClassroom: () => api.get("classroom/"),
    // getAllClassroomById: (classroomId) => api.get(`classroom/${classroomId}`),
  
    // //Schedule
    // createClassroom:(classroom) => api.post("/classrooms",ReservaController)


};

export default sheets;
