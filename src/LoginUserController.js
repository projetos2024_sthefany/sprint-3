import React, { useState } from 'react';
import { View, Text, Button, TextInput, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import sheets from './axios'; // Importa o módulo 'sheets' que contém as funções de conexão com a API

const LoginScreen = ({ onLogin }) => {
  const [nome, setNome] = useState('');
  const [senha, setSenha] = useState('');
  const [error, setError] = useState('');

  const navigation = useNavigation();

  const handleLogin = async () => {
    try {
      // Certifique-se de que 'nome' e 'senha' estão definidos corretamente
      const userData = {
        nome,
        senha,
      };

      // Faz a chamada à API para fazer o login do usuário
      const response = await sheets.logUser(userData); // Chama a função 'logUser' do módulo 'sheets' para efetuar o login do usuário

      // Verifica se o login foi bem-sucedido
      if (response.status === 200) {
        // Login bem-sucedido: você pode adicionar seu código aqui para navegar para outra tela,
        // ou salvar as informações do usuário, conforme necessário
        alert('Login realizado com sucesso!');
        // Exemplo: navegue para outra tela ou realize uma ação
        navigation.navigate('Users'); // Navega para a tela 'Users' após o login bem-sucedido
      } else {
        // Login falhou: mostre uma mensagem de erro ao usuário
        alert('Falha no login. Por favor, verifique suas credenciais.');
      }
    } catch (error) {
      // Lida com erros de rede ou outros problemas
      console.error('Erro durante o login:', error);
      setError('Erro durante o login. Por favor, tente novamente mais tarde.');
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../assets/imagem.jpg')}
        style={styles.backgroundImage}
      >
        <View style={styles.content}>
          {/* Botão de voltar */}
          <TouchableOpacity style={styles.buttonBack} onPress={() => navigation.goBack()}>
            <Text style={styles.buttonBackText}>Voltar</Text>
          </TouchableOpacity>
          
          <Text style={styles.title}>Faça Login</Text>
          <View style={styles.inputContainer}>
            <View style={styles.inputBox}>
              <TextInput
                style={styles.input}
                placeholder="Email"
                onChangeText={setNome}
                value={nome}
              />
            </View>
            <View style={styles.inputBox}>
              <TextInput
                style={styles.input}
                placeholder="Senha"
                onChangeText={setSenha}
                value={senha}
                secureTextEntry
              />
            </View>
            {error ? <Text style={styles.error}>{error}</Text> : null}
            <Button title="Entrar" onPress={handleLogin} />
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  content: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'black',
  },
  inputContainer: {
    marginBottom: 20,
    borderRadius: 10,
    backgroundColor: '#00447c',
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  inputBox: {
    marginBottom: 20,
    width: 200,
  },
  input: {
    width: 200,
    height: 40,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#00447c',
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
  buttonBack: {
    position: 'absolute',
    top: 10, 
    left: 10, 
    backgroundColor: '#00447c',
    padding: 10,
    borderRadius: 5,
  },
  buttonBackText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default LoginScreen;
